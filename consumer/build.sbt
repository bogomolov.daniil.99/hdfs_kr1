name := "hdfs-consumer"

version := "0.1"

scalaVersion := "2.13.7"


libraryDependencies += "com.rabbitmq" % "amqp-client" % "5.13.1"
libraryDependencies += "org.apache.hive" % "hive-jdbc" % "3.1.2"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.2"



idePackagePrefix := Some("ru.itis")
