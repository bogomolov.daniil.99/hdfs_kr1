package ru.itis

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Reads}

case class UserConnection(login: String, ipAddress: String, isSuccessfull: Boolean);

object UserConnection {
  implicit val userConnectionReads: Reads[UserConnection] = (
    (JsPath \ "login").read[String] and
      (JsPath \ "ip_address").read[String] and
      (JsPath \ "is_successfull").read[Boolean]
    )(UserConnection.apply _)

}
