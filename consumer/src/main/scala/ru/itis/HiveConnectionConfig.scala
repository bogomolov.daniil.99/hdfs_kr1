package ru.itis

import org.apache.hive.jdbc.HiveDriver

import java.sql.{Connection, DriverManager};

object HiveConnectionConfig {

  implicit def getConnection(): Connection = {
    DriverManager.getConnection("jdbc:hive2://62.84.123.85:10000", "hive", "");
  }
}
