package ru.itis

import com.rabbitmq.client.{CancelCallback, ConnectionFactory, DeliverCallback, Delivery}
import play.api.libs.json.Json

import java.sql.Connection

object RabbitMQConsumer {

  def configureConnection(): ConnectionFactory = {
    val HOST = "62.84.123.85"
    val PORT = 5672
    val USERNAME = "rabbit"
    val PASSWORD = "pass"

    val factory = new ConnectionFactory()
    factory.setHost(HOST)
    factory.setPort(PORT)
    factory.setUsername(USERNAME)
    factory.setPassword(PASSWORD)

    factory
  }

  def onMessage: DeliverCallback = (tag, delivery) => {
    val message = new String(delivery.getBody)
    sendToHive(HiveConnectionConfig.getConnection(), message)
  }

  def sendToHive(connection: Connection, data: String): Unit = {
    val jsonValue = Json.parse(data)
    println(jsonValue)
    val userConnection = Json.fromJson[UserConnection](jsonValue).get
    val sqlQuery = String.format("INSERT INTO user_connections VALUES('%s', '%s', %b)",
      userConnection.login, userConnection.ipAddress, userConnection.isSuccessfull)
    val statement = connection.createStatement()
    statement.execute(sqlQuery)
  }

  def cancel: CancelCallback = consumerTag => {}

  def main(args: Array[String]): Unit = {
    val QUEUE_NAME = "hdfs-data-queue"
    val connection = configureConnection().newConnection()
    val channelOptional = connection.openChannel()

    val channel = channelOptional.get()

    channel.basicConsume(QUEUE_NAME, true, onMessage, cancel)


    while (true) {

    }
    channel.close()
    connection.close()
  }

}
