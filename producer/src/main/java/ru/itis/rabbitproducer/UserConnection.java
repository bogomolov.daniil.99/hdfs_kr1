package ru.itis.rabbitproducer;

import java.util.Random;

public class UserConnection {
    private final String login;
    private final String ipAddress;
    private final Boolean isSuccessfull;

    public UserConnection() {
        login = generateRandomString();
        ipAddress = generateRandomIpAddress();
        isSuccessfull = generateRandomBoolean();
    }

    public String toJson() {
        String template = "{" +
                "\"login\" : \"%s\", " +
                "\"ip_address\" : \"%s\", " +
                "\"is_successfull\" : %b " +
                "}";
        return String.format(template, login, ipAddress, isSuccessfull);
    }

    private String generateRandomString() {
        Random r = new Random();
        return "user-" + String.valueOf(r.nextInt());
    }

    private String generateRandomIpAddress() {
        Random r = new Random();
        return r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256);
    }

    private boolean generateRandomBoolean() {
        Random r = new Random();
        return r.nextBoolean();
    }
}
