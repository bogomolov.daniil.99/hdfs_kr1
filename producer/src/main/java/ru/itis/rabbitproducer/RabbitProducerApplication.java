package ru.itis.rabbitproducer;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableRabbit
public class RabbitProducerApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(RabbitProducerApplication.class, args);
        RabbitTemplate template = context.getBean(RabbitTemplate.class);
        for (int i = 0; i < 1000; i++) {
            template.convertAndSend("hdfs-data-queue", new UserConnection().toJson());
        }
        System.out.println("Sent successfully");
    }

}
