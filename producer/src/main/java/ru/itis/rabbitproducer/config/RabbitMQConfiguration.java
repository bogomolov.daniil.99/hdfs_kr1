package ru.itis.rabbitproducer.config;

import com.rabbitmq.client.AMQP;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfiguration {

    @Bean
    public Queue queue() {
        return new Queue("hdfs-data-queue");
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setUsername("rabbit");
        connectionFactory.setPassword("pass");
        connectionFactory.setAddresses("62.84.123.85:5672");
        connectionFactory.setChannelCacheSize(10);
        return connectionFactory;
    }

}
